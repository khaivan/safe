package com.example.khailv.safe.layout2;

import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.khailv.safe.R;
import com.example.khailv.safe.layout2.adapter.AdapterHome;
import com.example.khailv.safe.layout2.model.ChildItem;
import com.example.khailv.safe.layout2.model.ParentItems;

import java.util.ArrayList;
import java.util.List;



public class HomeActivity extends AppCompatActivity {
    private TabLayout tabLayout;
    private RecyclerView rcView;
    private final static String TAG = HomeActivity.class.getSimpleName();
private List<ParentItems> parentItems;
private List<ChildItem> childItems;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);
        initsData();
        initsTablayout();
        initsRC();
    }


    private void initsData() {
        parentItems = new ArrayList<>();
        childItems = new ArrayList<>();
        childItems.add(new ChildItem("Sept 23, 2018",360.00,12.00));
        childItems.add(new ChildItem("Sept 23, 2018",360.00,12.00));
        childItems.add(new ChildItem("Sept 23, 2018",360.00,12.00));
        parentItems.add(new ParentItems("SEPTEMBER INCOME",341.75,childItems));
        parentItems.add(new ParentItems("AUGUST INCOME",341.75,childItems));
        parentItems.add(new ParentItems("JULY INCOME",341.75,childItems));
        parentItems.add(new ParentItems("SEPTEMBER",341.75,childItems));
    }

    private void initsRC() {
        rcView = (RecyclerView) findViewById(R.id.rc_view);
        AdapterHome adapterHome = new AdapterHome(parentItems);
        rcView.setLayoutManager(new LinearLayoutManager(this));
        rcView.setAdapter(adapterHome);
    }

    private void initsTablayout() {
         tabLayout = (TabLayout) findViewById(R.id.tablayout);
        tabLayout.addTab(tabLayout.newTab().setText("Backup"));
        tabLayout.addTab(tabLayout.newTab().setText("Restore"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.getTabAt(0).setCustomView(R.layout.tab_home);
        tabLayout.getTabAt(1).setCustomView(R.layout.tab_schedule);
    }





}
