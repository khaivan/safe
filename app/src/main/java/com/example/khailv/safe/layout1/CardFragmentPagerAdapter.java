package com.example.khailv.safe.layout1;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.widget.CardView;
import android.view.ViewGroup;

import com.example.khailv.safe.layout1.fragment.FirstFragment;
import com.example.khailv.safe.layout1.fragment.SeconFragment;
import com.example.khailv.safe.layout1.fragment.ThirdFragment;

import java.util.ArrayList;
import java.util.List;



public class CardFragmentPagerAdapter extends FragmentStatePagerAdapter implements CardAdapter {
    List<CardView> cardViews = new ArrayList<>();

    private List<Fragment> fragments;
    private float baseElevation;

    public CardFragmentPagerAdapter(FragmentManager fm, float baseElevation) {
        super(fm);
        fragments = new ArrayList<>();
        this.baseElevation = baseElevation;
        addCardFragment();


    }

    @Override
    public float getBaseElevation() {
        return baseElevation;
    }

    @Override
    public CardView getCardViewAt(int position) {
        if (position == 0) {
            FirstFragment firstFragment = (FirstFragment) fragments.get(0);
            return firstFragment.getCardView();
        }
        if (position == 1) {
            SeconFragment seconFragment = (SeconFragment) fragments.get(1);
            return seconFragment.getCardView();
        } else {

            ThirdFragment thirdFragment = (ThirdFragment) fragments.get(2);
            return thirdFragment.getCardView();

        }

    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Object fragment = super.instantiateItem(container, position);
        fragments.set(position, (Fragment) fragment);
        return fragment;
    }

    public void addCardFragment() {

        fragments.add(new FirstFragment());
        fragments.add(new SeconFragment());
        fragments.add(new ThirdFragment());


    }

}
