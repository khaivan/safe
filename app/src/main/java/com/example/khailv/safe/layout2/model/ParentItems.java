package com.example.khailv.safe.layout2.model;

import android.os.Parcel;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class ParentItems extends ExpandableGroup {
    private String month;
    private Double money;

    public ParentItems(String title,  Double money,List items ) {
        super(title, items);
        this.money = money;
    }


    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }
}
