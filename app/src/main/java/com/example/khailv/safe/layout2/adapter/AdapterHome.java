package com.example.khailv.safe.layout2.adapter;

import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.khailv.safe.R;
import com.example.khailv.safe.layout2.model.ChildItem;
import com.example.khailv.safe.layout2.model.ParentItems;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import java.util.List;



public class AdapterHome extends ExpandableRecyclerViewAdapter<AdapterHome.ParentViewHolder, AdapterHome.ChildViewHolder> {
    private IGetItemHome iGetItemHome;
private ParentItems parentItems;
private View view;
    public AdapterHome(List<? extends ExpandableGroup> groups) {
        super(groups);
    }

    @Override
    public AdapterHome.ParentViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.parent_item, parent, false);
        return new ParentViewHolder(view);
    }

    @Override
    public AdapterHome.ChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.child_items, parent, false);
        return new ChildViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(AdapterHome.ChildViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        ChildItem childItem = (ChildItem) group.getItems().get(childIndex);
        holder.tvDateChild.setText(childItem.getDate());
        holder.tvMoneyChild.setText(String.valueOf(childItem.getMoney()));
        holder.tvMoneyWith.setText(String.valueOf(childItem.getWitheld()));
    }

    @Override
    public void onBindGroupViewHolder(AdapterHome.ParentViewHolder holder, int flatPosition, ExpandableGroup group) {
        if (parentItems==null){
            parentItems = (ParentItems) getGroups().get(flatPosition);
        }
        holder.tvMonth.setText(""+group.getTitle());
        holder.tvMoney.setText("$"+parentItems.getMoney());
    }


    class ParentViewHolder extends GroupViewHolder {
        private TextView tvMonth, tvMoney;
        private CardView rcBtn;

        public ParentViewHolder(View itemView) {
            super(itemView);
            tvMonth = (TextView) itemView.findViewById(R.id.tv_month);
            tvMoney = (TextView) itemView.findViewById(R.id.tv_money);
            rcBtn = (CardView) itemView.findViewById(R.id.cv_septem);
        }
    }

    class ChildViewHolder extends com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder {
        private TextView tvDateChild, tvMoneyChild, tvMoneyWith;

        public ChildViewHolder(View itemView) {
            super(itemView);
            tvDateChild = (TextView) itemView.findViewById(R.id.tv_month_child);
            tvMoneyChild = (TextView) itemView.findViewById(R.id.tv_money_child);
            tvMoneyWith = (TextView) itemView.findViewById(R.id.tv_money_witheld);
        }
    }

    public interface IGetItemHome {

        void onClickItem(int position);

    }
}
