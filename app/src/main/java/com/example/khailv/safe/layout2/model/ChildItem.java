package com.example.khailv.safe.layout2.model;

public class ChildItem {
    private String date;
    private Double money;
    private Double witheld;

    public ChildItem(String date, Double money, Double witheld) {
        this.date = date;
        this.money = money;
        this.witheld = witheld;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public Double getWitheld() {
        return witheld;
    }

    public void setWitheld(Double witheld) {
        this.witheld = witheld;
    }
}
